 
# Rabodegalo design
        |-- 🐓 rabodegalo
        | |-- 📁 app
        | | |-- 📄 main.sh
        | | |-- 📄 request_handler.sh
        | | |-- 📄 transaction_handler.sh
        | | |-- 📁 config
        | | | |-- 📄 init.sql
        | | | |-- 📄 postgresql.conf
        | | | |-- 📄 nginx.conf
        | | | |-- 📁 views
        | | | |-- 📄 404.htmlr
        | | | |-- 📄 bank_statement.jsonr
        | | | |-- 📄 transactions.jsonr
        | |-- 📄 Dockerfile
        | |-- 📄 docker-compose.yml
          |-- 📁 stress-test
          |-- 📄 404.htmlr
          |---- 📁 user-files
          |------- 📁 user-files
          |---------- 📁 simulations
          |------------- 📄 RinhaBackendSimulation.scala

  


# Requisitos baseado em teste stress

Validação de criação de transações (débitos e créditos)
Status code 200 com dados válidos
Validação dos campos obrigatórios
Validação dos tipos de dados
Validação de valor máximo de transação
Atualização correta do saldo e limite
Consulta de extratos
Status code 200 com dados válidos
Retorno de últimas transações feitas
Consistência do saldo total com o histórico de transações
Consistência do limite
Concorrência de transações simultâneas
Inserção e validação de 25 transações por segundo do mesmo tipo
Verificação do saldo total após a operação
Validação de requisições concorrentes
5 consultas simultâneas do extrato com dados consistentes
Validação de erros
Status code 404 para cliente não encontrado
Status codes 422/400 para dados de transação inválidos
Métricas
Tempo médio de resposta
Taxa de sucesso
Erros por segundo
Com esses requisitos mapeados, precisamos desenhar os casos de teste funcional cobrindo todos os cenários, criar dados de entrada e validações para cada um. Em seguida, implementar os testes unitários em um framework de testes e automatizar a execução. Por fim, rodaremos os testes de carga e performance para validar a escalabilidade sob alta demanda.

Casos de teste funcionais:

Testar obtenção válida de ID do cliente
Testar tratamento de ID de cliente inválido
Testar transação válida (crédito/débito)
Testar valor de transação inválido
Testar tipo de transação inválido
Testar atualização de saldo e histórico na transação
Testar validação de saldo na transação
Testar obtenção de extrato para cliente válido
Testar erro em extrato para cliente inválido
Framework para testes unitários:


Testes Funcionais

Criar dados de teste para clientes válidos e inválidos, valores/tipos de transação, descrições
Testes para criação com sucesso de crédito/débito cobrindo todos os campos e regras de negócio
Testes para falha na criação de crédito/débito devido a dados inválidos ou limites excedidos
Busca de extratos para cliente válido retornar dados e formatos esperados
Busca falha ou retorna vazio para cliente inválido
Testes Unitários

Modularizar código em unidades testáveis como funções para operações específicas
Usar framework de teste unitário como CUnit para chamar funções com inputs diferentes
Validar saídas e erros esperados são retornados
Testes de Concorrência

Simular requisições concorrentes aos endpoints usando threads/processos
Testar integridade de dados quando mesmos recursos são acessados simultaneamente
Validar controles de concorrência e locks previnem inconsistências
Testes de Carga/Estresse

Automatizar testes para simular altos volumes de tráfego ao longo do tempo
Monitorar performance do sistema e APIs para tempos de resposta e erros
Encontrar ponto de ruptura quando sistema não aguenta mais aumento de carga
Identificar gargalos e otimizar configuração conforme necessário para melhorar escalabilidade
Benchmarks de Performance

Definir SLAs para usuários e transações concurrentes por segundo esperados
Testes de performance ajudam a verificar se sistema atende requisitos sob alta utilização

# requisitos baseados em regras de negócios


Baseado nos requisitos fornecidos na documentação da Rinha de Backend, eis os requisitos chaves que precisamos considerar além dos requisitos de teste de carga já mencionados:

Requisitos Funcionais:

A API deve suportar endpoints de criação de transações para crédito e débito
As transações devem validar campos obrigatórios, tipos de dados e regras de negócio
As atualizações no saldo e limite do cliente após a transação devem estar corretas
A API deve suportar endpoint de consulta de extratos para clientes
A resposta da consulta deve incluir o histórico das últimas transações e os totais atuais
As APIs devem retornar códigos de erro adequados para requisições inválidas
Requisitos Não Funcionais:

As APIs devem suportar um nível mínimo de concorrência para transações (25 TPS)
Várias requisições de consulta simultâneas devem retornar dados consistentes
As APIs devem atender às metas de tempo médio de resposta sob carga
O sistema deve ser escalonável para aumentar o throughput com mais recursos
Outras Considerações:

A configuração inicial de dados de clientes é pré-definida para limitar variáveis
A arquitetura mínima de balanceador de carga e duas instâncias de API é definida
Restrições de recursos de 1,5 CPUs e 550MB de memória se aplicam
Os testes validarão contratos de endpoints e tratamento de erros
Os testes de carga validarão performance, concorrência e escalabilidade
Para atender a esses requisitos, precisaremos:

Modelos de entidade e código de acesso a dados para clientes e transações
Código de camada de serviço implementando lógica de transação e consulta
Controladores de API expondo endpoints e validando requisições
Testes cobrindo funcionalidade, validação, casos de erro e concorrência
Configuração de balanceador de carga e instâncias de API conforme arquitetura
Esquema e scripts de inicialização do banco de dados
Configuração Docker respeitando restrições de recursos

Regras de negócio para validação de transações:

Uma transação de débito nunca pode deixar o saldo do cliente menor que seu limite disponível. Por exemplo, um cliente com limite de 1000 (R$ 10) nunca deverá ter o saldo menor que -1000 (R$ -10).

Os valores das transações devem ser números inteiros positivos representando centavos.

O tipo da transação deve ser apenas 'c' para crédito ou 'd' para débito.

A descrição deve ter entre 1 e 10 caracteres.

Campos como valor, tipo e descrição são obrigatórios.

Garantir dados consistentes em consultas simultâneas:

Realizar várias consultas ao extrato ao mesmo tempo para verificar se todos retornam os mesmos dados consistentes (saldo, extrato, limite etc).

Validar dados como saldo total, últimas transações, limite etc são os mesmos em todas as respostas.

Metas de tempo de resposta sob carga:

Não há metas de SLAs especificadas no documento. Para APIs sob carga, o objetivo é que o tempo médio de resposta se mantenha estável e dentro de valores razoáveis (alguns segundos no máximo) conforme a carga aumenta.

Eventualmente o tempo médio pode aumentar levemente, mas não de forma desproporcional ou que indique problemas de capacidade ou falhas nas APIs.

      #rabodegalo/stress-test/run-test.sh

      #!/usr/bin/bash
      # Use este script para executar testes locais

      RESULTS_WORKSPACE="$(pwd)/load-test/user-files/results"
      GATLING_BIN_DIR=$HOME/gatling/3.10.3/bin
      GATLING_WORKSPACE="$(pwd)/load-test/user-files"


      runGatling() {
          $GATLING_BIN_DIR/gatling.sh -rm local -s RinhaBackendSimulation \
              -rd "Rinha de Backend - 2024/Q1: Crébito" \
              -rf $WORKSPACE/user-files/results \
              -sf $WORKSPACE/user-files/simulations
      }

      startTest() {
          for i in {1..20}; do
              # 2 requests to wake the 2 api instances up :)
              curl --fail https://9999-cs-fa481776-aedf-42a6-980d-943bca6e762f.cs-us-east1-vpcf.cloudshell.dev/clientes/1/extrato && \
              echo "" && \
              curl --fail https://9999-cs-fa481776-aedf-42a6-980d-943bca6e762f.cs-us-east1-vpcf.cloudshell.dev/clientes/1/extrato && \
              echo "" && \
              runGatling && \
              break || sleep 2;
          done
      }

      startTest




      #rabodegalo/stress-test/user-files/simulations/rinhabackend/RinhaBackendCrebitosSimulation.scala

      import scala.concurrent.duration._
      import scala.util.Random
      import util.Try
      import io.gatling.commons.validation._
      import io.gatling.core.session.Session
      import io.gatling.core.Predef._


      class RinhaBackendCrebitosSimulation
        extends Simulation {

        def randomClienteId() = Random.between(1, 5 + 1)
        def randomValorTransacao() = Random.between(1, 10000 + 1)
        def randomDescricao() = Random.alphanumeric.take(10).mkString
        def randomTipoTransacao() = Seq("c", "d", "d")(Random.between(0, 2 + 1)) // not used
        def toInt(s: String): Option[Int] = {
          try {
            Some(s.toInt)
          } catch {
            case e: Exception => None
          }
        }

        val validarConsistenciaSaldoLimite = (valor: Option[String], session: Session) => {
          /*
            Essa função é frágil porque depende que haja uma entrada
            chamada 'limite' com valor conversível para int na session
            e também que seja encadeada com com jmesPath("saldo") para
            que 'valor' seja o primeiro argumento da função validadora
            de 'validate(.., ..)'.
            
            =============================================================
            
            Nota para quem não tem experiência em testes de performance:
              O teste de lógica de saldo/limite extrapola o que é comumente 
              feito em testes de performance apenas por causa da natureza
              da Rinha de Backend. Evite fazer esse tipo de coisa em 
              testes de performance, pois não é uma prática recomendada
              normalmente.
          */ 

          val saldo = valor.flatMap(s => Try(s.toInt).toOption)
          val limite = toInt(session("limite").as[String])

          (saldo, limite) match {
            case (Some(s), Some(l)) if s.toInt < l.toInt * -1 => Failure("Limite ultrapassado!")
            case (Some(s), Some(l)) if s.toInt >= l.toInt * -1 => Success(Option("ok"))
            case _ => Failure("WTF?!")
          }
        }

        val httpProtocol = http
          .baseUrl("http://localhost:9999")
          .userAgentHeader("Agente do Caos - 2024/Q1")

        val debitos = scenario("débitos")
          .exec {s =>
            val descricao = randomDescricao()
            val cliente_id = randomClienteId()
            val valor = randomValorTransacao()
            val payload = s"""{"valor": ${valor}, "tipo": "d", "descricao": "${descricao}"}"""
            val session = s.setAll(Map("descricao" -> descricao, "cliente_id" -> cliente_id, "payload" -> payload))
            session
          }
          .exec(
            http("débitos")
            .post(s => s"/clientes/${s("cliente_id").as[String]}/transacoes")
                .header("content-type", "application/json")
                .body(StringBody(s => s("payload").as[String]))
                .check(
                  status.in(200, 422),
                  status.saveAs("httpStatus"))
                .checkIf(s => s("httpStatus").as[String] == "200") { jmesPath("limite").saveAs("limite") }
                .checkIf(s => s("httpStatus").as[String] == "200") {
                  jmesPath("saldo").validate("ConsistenciaSaldoLimite - Transação", validarConsistenciaSaldoLimite)
                }
          )

        val creditos = scenario("créditos")
          .exec {s =>
            val descricao = randomDescricao()
            val cliente_id = randomClienteId()
            val valor = randomValorTransacao()
            val payload = s"""{"valor": ${valor}, "tipo": "c", "descricao": "${descricao}"}"""
            val session = s.setAll(Map("descricao" -> descricao, "cliente_id" -> cliente_id, "payload" -> payload))
            session
          }
          .exec(
            http("créditos")
            .post(s => s"/clientes/${s("cliente_id").as[String]}/transacoes")
                .header("content-type", "application/json")
                .body(StringBody(s => s("payload").as[String]))
                .check(
                  status.in(200),
                  jmesPath("limite").saveAs("limite"),
                  jmesPath("saldo").validate("ConsistenciaSaldoLimite - Transação", validarConsistenciaSaldoLimite)
                )
          )

        val extratos = scenario("extratos")
          .exec(
            http("extratos")
            .get(s => s"/clientes/${randomClienteId()}/extrato")
            .check(
              jmesPath("saldo.limite").saveAs("limite"),
              jmesPath("saldo.total").validate("ConsistenciaSaldoLimite - Extrato", validarConsistenciaSaldoLimite)
          )
        )

        val validacaConcorrentesNumRequests = 25
        val validacaoTransacoesConcorrentes = (tipo: String) =>
          scenario(s"validação concorrência transações - ${tipo}")
          .exec(
            http("validações")
            .post(s"/clientes/1/transacoes")
                .header("content-type", "application/json")
                .body(StringBody(s"""{"valor": 1, "tipo": "${tipo}", "descricao": "validacao"}"""))
                .check(status.is(200))
          )
        
        val validacaoTransacoesConcorrentesSaldo = (saldoEsperado: Int) =>
          scenario(s"validação concorrência saldo - ${saldoEsperado}")
          .exec(
            http("validações")
            .get(s"/clientes/1/extrato")
            .check(
              jmesPath("saldo.total").ofType[Int].is(saldoEsperado)
            )
          )

        val saldosIniciaisClientes = Array(
          Map("id" -> 1, "limite" ->   1000 * 100),
          Map("id" -> 2, "limite" ->    800 * 100),
          Map("id" -> 3, "limite" ->  10000 * 100),
          Map("id" -> 4, "limite" -> 100000 * 100),
          Map("id" -> 5, "limite" ->   5000 * 100),
        )

        val criterioClienteNaoEcontrado = scenario("validação HTTP 404")
          .exec(
            http("validações")
            .get("/clientes/6/extrato")
            .check(status.is(404))
          )

        val criteriosClientes = scenario("validações")
          .feed(saldosIniciaisClientes)
          .exec(
            /*
              Os valores de http(...) essão duplicados propositalmente
              para que sejam agrupados no relatório e ocupem menos espaço.
              O lado negativo é que, em caso de falha, pode não ser possível
              saber sua causa exata.
            */ 
            http("validações")
            .get("/clientes/#{id}/extrato")
            .check(
              status.is(200),
              jmesPath("saldo.limite").ofType[String].is("#{limite}"),
              jmesPath("saldo.total").ofType[String].is("0")
            )
          )
          .exec(
            http("validações")
            .post("/clientes/#{id}/transacoes")
                .header("content-type", "application/json")
                .body(StringBody(s"""{"valor": 1, "tipo": "c", "descricao": "toma"}"""))
                .check(
                  status.in(200),
                  jmesPath("limite").saveAs("limite"),
                  jmesPath("saldo").validate("ConsistenciaSaldoLimite - Transação", validarConsistenciaSaldoLimite)
                )
          )
          .exec(
            http("validações")
            .post("/clientes/#{id}/transacoes")
                .header("content-type", "application/json")
                .body(StringBody(s"""{"valor": 1, "tipo": "d", "descricao": "devolve"}"""))
                .check(
                  status.in(200),
                  jmesPath("limite").saveAs("limite"),
                  jmesPath("saldo").validate("ConsistenciaSaldoLimite - Transação", validarConsistenciaSaldoLimite)
                )
          )
          .exec(
            http("validações")
            .get("/clientes/#{id}/extrato")
            .check(
              jmesPath("ultimas_transacoes[0].descricao").ofType[String].is("devolve"),
              jmesPath("ultimas_transacoes[0].tipo").ofType[String].is("d"),
              jmesPath("ultimas_transacoes[0].valor").ofType[Int].is("1"),
              jmesPath("ultimas_transacoes[1].descricao").ofType[String].is("toma"),
              jmesPath("ultimas_transacoes[1].tipo").ofType[String].is("c"),
              jmesPath("ultimas_transacoes[1].valor").ofType[Int].is("1")
          )
        )
        .exec(
            http("validações")
            .post("/clientes/#{id}/transacoes")
                .header("content-type", "application/json")
                .body(StringBody(s"""{"valor": 1.2, "tipo": "d", "descricao": "devolve"}"""))
                .check(status.in(422))
          )
          .exec(
            http("validações")
            .post("/clientes/#{id}/transacoes")
                .header("content-type", "application/json")
                .body(StringBody(s"""{"valor": 1, "tipo": "x", "descricao": "devolve"}"""))
                .check(status.in(422))
          )
          .exec(
            http("validações")
            .post("/clientes/#{id}/transacoes")
                .header("content-type", "application/json")
                .body(StringBody(s"""{"valor": 1, "tipo": "c", "descricao": "123456789 e mais um pouco"}"""))
                .check(status.in(422))
          )
          .exec(
            http("validações")
            .post("/clientes/#{id}/transacoes")
                .header("content-type", "application/json")
                .body(StringBody(s"""{"valor": 1, "tipo": "c", "descricao": ""}"""))
                .check(status.in(422))
          )
          .exec(
            http("validações")
            .post("/clientes/#{id}/transacoes")
                .header("content-type", "application/json")
                .body(StringBody(s"""{"valor": 1, "tipo": "c", "descricao": null}"""))
                .check(status.in(422))
          )

        /* 
          Separar créditos e débitos dá uma visão
          melhor sobre como as duas operações se
          comportam individualmente.
        */
        setUp(
          validacaoTransacoesConcorrentes("d").inject(
            atOnceUsers(validacaConcorrentesNumRequests)
          ).andThen(
            validacaoTransacoesConcorrentesSaldo(validacaConcorrentesNumRequests * -1).inject(
              atOnceUsers(1)
            )
          ).andThen(
            validacaoTransacoesConcorrentes("c").inject(
              atOnceUsers(validacaConcorrentesNumRequests)
            ).andThen(
              validacaoTransacoesConcorrentesSaldo(0).inject(
                atOnceUsers(1)
              )
            )
          ).andThen(
            criteriosClientes.inject(
              atOnceUsers(saldosIniciaisClientes.length)
            ),
            criterioClienteNaoEcontrado.inject(
              atOnceUsers(1)
            ).andThen(
              debitos.inject(
                rampUsersPerSec(1).to(220).during(2.minutes),
                constantUsersPerSec(220).during(2.minutes)
              ),
              creditos.inject(
                rampUsersPerSec(1).to(110).during(2.minutes),
                constantUsersPerSec(110).during(2.minutes)
              ),
              extratos.inject(
                rampUsersPerSec(1).to(10).during(2.minutes),
                constantUsersPerSec(10).during(2.minutes)
              )
            )
          )
        ).protocols(httpProtocol)
      }

      # imagem docker app rabodegalo
      #rabodegalo/app/main.sh
      #!/bin/bash

      # Define o PID do processo atual
      PID=$$

      # Cria o FIFO para comunicação entre processos
      FIFO_PATH="/tmp/pid-$PID/response"
      rm -f $FIFO_PATH
      mkfifo $FIFO_PATH

      # Importa as funções de manipulação de solicitações
      source request_handler.sh

      # Valida entradas antes de chamar a função sensível
      if [[ -n "${session["valor"]}" && -n "${session["limite"]}" ]]; then
          validarConsistenciaSaldoLimite session["valor"] session
      else
          # Trate o caso em que 'valor' ou 'limite' estão ausentes na sessão
          echo "Valor ou limite ausentes na sessão"
      fi

      # Inicia o servidor
      echo 'Listening on 3000...'
      nc -lN 3000 < $FIFO_PATH | while read -r request; do
          handleRequest "$request" > $FIFO_PATH &
      done


      #rabodegalo/app/request_handler.sh
      #!/bin/bash

      # Parses the request and invokes appropriate handler
      function handleRequest() {
        read -r -a request <<< "$1"

        # Parses the path parameter (integer)
        local path_regex='GET /clientes/([0-9]+)/extrato$'
        if [[ "${request[1]}" =~ $path_regex ]]; then
          PARAMS["id"]="${BASH_REMATCH[1]}"
          handle_GET_bank_statement
        elif [[ "${request[1]}" == "POST /clientes/:id/transacoes" ]]; then
          handle_POST_transactions
        else
          handle_not_found
        fi
      }

      #rabodegalo/app/transaction_handler.sh
      #!/bin/bash

      # Handles GET request for bank statement
      function handle_GET_bank_statement() {
        local id="${PARAMS["id"]}"

        if [[ -n "$id" ]]; then
          local query="
            WITH ten_transactions AS (
              SELECT * FROM transactions 
              WHERE account_id = $id 
              ORDER BY date DESC
              LIMIT 10
            )
            SELECT 
              json_build_object('saldo', json_build_object(
                'total', balances.amount,
                'data_extrato', NOW()::date,
                'limite', accounts.limit_amount,
                'ultimas_transacoes', 
                  CASE 
                  WHEN COUNT(transactions) = 0 THEN '[]'
                  ELSE
                    json_agg(
                      json_build_object(
                        'valor', transactions.amount,
                        'tipo', transactions.transaction_type,
                        'descricao', transactions.description,
                        'realizada_em', transactions.date::date
                      )
                    )
                  END
              ))
            FROM accounts
            LEFT JOIN balances ON balances.account_id = accounts.id
            LEFT JOIN ten_transactions AS transactions ON transactions.account_id = accounts.id
            WHERE accounts.id = $id
            GROUP BY accounts.id, balances.amount, accounts.limit_amount
          "

          # Executa a consulta e processa a resposta
          local result=$(psql -t -h pgbouncer -U postgres -d postgres -p 6432 -c "$query" | tr -d '[:space:]') 

          if [[ -n "$result" ]]; then
            generate_response "$result"
          else
            handle_not_found
          fi
        fi
      }


      # Handles POST request for transactions
      function handle_POST_transactions() {
        local id="${PARAMS["id"]}"
        local amount=$(jq -r '.valor' <<< "$BODY")
        local transaction_type=$(jq -r '.tipo' <<< "$BODY")
        local description=$(jq -r '.descricao' <<< "$BODY")

        local operation
        [[ "$transaction_type" == "c" ]] && operation="+" || operation="-"

        if [[ -n "$id" ]]; then
          local query="
            INSERT INTO transactions (account_id, amount, description, transaction_type)
            VALUES ($id, $amount, '$description', '$transaction_type');

            UPDATE balances
            SET amount = amount $operation $amount
            WHERE balances.account_id = $id;

            SELECT 
              json_build_object(
                'limite', accounts.limit_amount,
                'saldo', balances.amount
              )
            FROM accounts
            LEFT JOIN balances ON balances.account_id = accounts.id
            WHERE account_id = $id
          "

          # Executa a consulta e processa a resposta
          local result=$(psql -t -h pgbouncer -U postgres -d postgres -p 6432 -c "$query" | tr -d '[:space:]') 

          if [[ -n "$result" ]]; then
            generate_response "$result"
          else
            handle_not_found
          fi
        fi
      }

      function handle_GET_bank_statement() {
        ID=${PARAMS["id"]}

        if [ ! -z "$ID" ]; then
          # Sanitize input: ensure ID is numeric
          if ! [[ $ID =~ ^[0-9]+$ ]]; then
            handle_invalid_input "Invalid ID format"
            return
          fi

          QUERY="
      WITH ten_transactions AS (
          SELECT * FROM transactions 
          WHERE account_id = $ID 
          ORDER BY date DESC
          LIMIT 10
      )
      SELECT 
        json_build_object('saldo', json_build_object(
          'total', balances.amount,
          'data_extrato', NOW()::date,
          'limite', accounts.limit_amount,
          'ultimas_transacoes', 
            CASE 
            WHEN COUNT(transactions) = 0 THEN '[]'
            ELSE
              json_agg(
                json_build_object(
                  'valor', transactions.amount,
                  'tipo', transactions.transaction_type,
                  'descricao', transactions.description,
                  'realizada_em', transactions.date::date
                )
              )
            END
        ))
      FROM accounts
      LEFT JOIN balances ON balances.account_id = accounts.id
      LEFT JOIN ten_transactions AS transactions ON transactions.account_id = accounts.id
      WHERE accounts.id = $ID
      GROUP BY accounts.id, balances.amount, accounts.limit_amount"

          RESULT=$(psql -t -h pgbouncer -U postgres -d postgres -p 6432 -c "$QUERY" | tr -d '[:space:]') 

          if [ ! -z "$RESULT" ]; then
            generate_response "$RESULT"
          else
            handle_not_found
          fi
        fi
      }


      #rabodegalo/app/response_handler.sh
      #!/bin/bash

      # Generates response based on the result
      function generate_response() {
        local result="$1"

        if [[ -n "$result" ]]; then
          RESPONSE=$(cat views/bank_statement.jsonr | sed "s/{{data}}/$result/")
        else
          RESPONSE=$(cat views/404.htmlr)
        fi
      }

      # Handles not found error
      function handle_not_found() {
        RESPONSE=$(cat views/404.htmlr)
      }


      #rabodegalo/postgresql.conf

      # -----------------------------
      # PostgreSQL configuration file
      # -----------------------------

      listen_addresses = '*'

      # RESOURCE USAGE
      max_connections = 300

      # CHECKPOINTER
      checkpoint_completion_target = 0.9    # Define a porcentagem do trabalho que o checkpointer tenta realizar entre pontos de verificação, ajuste conforme necessário
      checkpoint_flush_after = 256kB    # Define o limite de buffer após o qual o checkpointer escreve os dados no disco, ajuste conforme necessário
      checkpoint_timeout = 0    # Desativa a escrita periódica de pontos de verificação no disco, escrevendo na memória

      # BACKGROUND WRITER
      bgwriter_delay = 10ms    # Define o atraso entre as gravações feitas pelo background writer, ajuste conforme necessário
      bgwriter_lru_maxpages = 100    # Define o número máximo de páginas sujas que o background writer tentará escrever por vez, ajuste conforme necessário
      bgwriter_lru_multiplier = 2.0    # Define a quantidade de páginas a serem escritas por vez, ajuste conforme necessário
      bgwriter_flush_after = 256kB    # Define o limite de buffer após o qual o background writer escreve os dados no disco, ajuste conforme necessário

      # WRITING
      fsync = off    # Desativa a confirmação de que os dados foram gravados no disco antes de confirmar uma transação
      synchronous_commit = off    # Desativa a espera pela confirmação de que os dados foram gravados no disco antes de confirmar uma transação
      full_page_writes = off    # Quantidade de dados gravados no disco escrevendo páginas de dados modificadas de forma incremental
      max_wal_senders = 5    # Aumenta o número máximo de remetentes de log wal para 5
      wal_level = hot_standby

      # WALWRITER
      wal_writer_delay = 200ms    # Define o atraso entre as gravações feitas pelo walwriter, ajuste conforme necessário
      wal_writer_flush_after = 1MB    # Define o limite de buffer após o qual o walwriter escreve os dados no disco, ajuste conforme necessário

      # AUTOVACUUM LAUNCHER
      autovacuum = on    # Ativa o autovacuum
      autovacuum_max_workers = 3    # Define o número máximo de processos de autovacuum
      autovacuum_naptime = 1min    # Define o intervalo de tempo entre as verificações de autovacuum
      autovacuum_vacuum_scale_factor = 0.2    # Define a porcentagem de tamanho de tabela que aciona uma operação de VACUUM
      autovacuum_analyze_scale_factor = 0.1    # Define a porcentagem de tamanho de tabela que aciona uma operação de ANALYZE
      autovacuum_vacuum_threshold = 50    # Define o número mínimo de tuplas obsoletas necessárias para acionar uma operação de VACUUM
      autovacuum_analyze_threshold = 50    # Define o número mínimo de tuplas obsoletas necessárias para acionar uma operação de ANALYZE

      # LOGICAL REPLICATION LAUNCHER
      max_replication_slots = 10    # Define o número máximo de slots de replicação

      # QUERY TUNING
      random_page_cost = 1.0    # Reduz o custo de leitura de páginas aleatórias para 1.0
      effective_io_concurrency = 200    # Aumenta a concorrência de E/S efetiva para 200

      # ESG 
      shared_buffers = 0.25    # buffers compartilhados para 0.25
      work_mem = 0.65    # Aumenta a memória de trabalho
      maintenance_work_mem = 0.50    # Aumenta a memória de manutenção
      effective_cache_size = 0.50    # Ajusta o tamanho do cache efetivo

      # Multi-tenancy Postres


      #rabodegalo/config/init.sql

      -- Criação da tabela de contas
      CREATE TABLE IF NOT EXISTS accounts (
          id SERIAL PRIMARY KEY,
          name VARCHAR(50) NOT NULL,
          limit_amount INTEGER NOT NULL
      );

      -- Criação da tabela de transações
      CREATE TABLE IF NOT EXISTS transactions (
          id SERIAL PRIMARY KEY,
          account_id INTEGER NOT NULL,
          amount INTEGER NOT NULL,
          transaction_type CHAR(1) NOT NULL,
          description VARCHAR(255) NOT NULL,
          date TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
          FOREIGN KEY (account_id) REFERENCES accounts(id)
      );

      -- Criação da tabela de saldos
      CREATE TABLE IF NOT EXISTS balances (
          account_id INTEGER PRIMARY KEY REFERENCES accounts(id),
          amount INTEGER NOT NULL,
          FOREIGN KEY (account_id) REFERENCES accounts(id)
      );

      -- Inicialização dos dados com o uso de transação
      DO $$
      BEGIN
          -- Inicia uma transação
          BEGIN
              -- Inserção de contas com seus limites
              INSERT INTO accounts (name, limit_amount)
              VALUES
                  ('o barato sai caro', 100000),
                  ('zan corp ltda', 80000),
                  ('les cruders', 1000000),
                  ('padaria joia de cocaia', 10000000),
                  ('kid mais', 500000)
              RETURNING id;

              -- Inserção de saldos iniciais para cada conta
              INSERT INTO balances (account_id, amount)
              SELECT id, 0 FROM accounts;
          -- Finaliza a transação
          EXCEPTION
              WHEN others THEN
                  -- Em caso de erro, faz rollback da transação
                  ROLLBACK;
          END;
          -- Commit da transação
          COMMIT;
      END $$;


      #rabodegalo/views/404.htmlr
      HTTP/1.1 404 NotFound
      Content-Type: text/html

      <h1>Sorry, not found</h1>

      #rabodegalo/views/bank_statement.jsonr
      HTTP/1.1 200
      Content-Type: application/json

      {{data}}

      #rabodegalo/views/transactions.jsonr
      HTTP/1.1 200
      Content-Type: application/json

      {{data}}


      #rabodegalo/Dockerfile
      # Use a imagem base do Bash
      FROM bash:latest

      # Copie os arquivos de código para o contêiner
      COPY . /app

      # Defina o diretório de trabalho
      WORKDIR /app

      # Comando padrão para iniciar a API
      CMD ["bash", "main.sh"]



      #rabodegalo/docker-compose.yml
      version: '3'

      services:
        api1:
          build: 
            context: rabodegalo/app
            dockerfile: Dockerfile
          container_name: api1
          volumes:
            - .:/app
          depends_on:
            - postgres
            - pgbouncer
          deploy:
            replicas: 2 # Duas réplicas para balanceamento de carga
            resources:
              limits:
                cpus: '0.2'
                memory: '100MB'

        postgres:
          image: postgres
          container_name: postgres
          environment:
            - POSTGRES_PASSWORD=postgres
          ports:
            - 5432:5432
          volumes:
            - ./config/init.sql:/docker-entrypoint-initdb.d/init.sql
            - ./config/postgresql.conf:/etc/postgresql/postgresql.conf
          command: postgres -c config_file=/etc/postgresql/postgresql.conf
          deploy:
            resources:
              limits:
                cpus: '0.7'
                memory: '200MB'

        pgbouncer:
          image: pgbouncer/pgbouncer
          hostname: pgbouncer
          container_name: pgbouncer
          environment:
            - DATABASES_HOST=postgres
            - DATABASES_PORT=5432
            - DATABASES_USER=postgres
            - DATABASES_PASSWORD=postgres
            - DATABASES_DBNAME=postgres
            - DATABASES_POOL_SIZE=100
            - PGBOUNCER_MAX_CLIENT_CONN=100
            - PGBOUNCER_RESERVE_POOL_SIZE=100
            - PGBOUNCER_DEFAULT_POOL_SIZE=100
          depends_on:
            - postgres
          deploy:
            resources:
              limits:
                cpus: '0.2'
                memory: '100MB'

        nginx:
          image: nginx 
          container_name: nginx
          volumes:
            - ./config/nginx.conf:/etc/nginx/nginx.conf:ro
          ports:
            - 9999:9999
          depends_on:
            - api1
          deploy:
            resources:
              limits:
                cpus: '0.2'
                memory: '50MB'

                
      #rabodegalo/nginx.conf

      nginx:
          image: nginx 
          container_name: nginx
          volumes:
            - ./config/nginx.conf:/etc/nginx/nginx.conf:ro
          ports:
            - 9999:80
          depends_on:
            - api1
          deploy:
            resources:
              limits:
                cpus: '0.2'
                memory: '50MB'



      #!/usr/bin/bash

      # Use este script para executar testes locais

      RESULTS_WORKSPACE="$(pwd)/load-test/user-files/results"
      GATLING_BIN_DIR=$HOME/gatling/3.10.3/bin
      GATLING_WORKSPACE="$(pwd)/load-test/user-files"

      runGatling() {
        sudo sh $GATLING_BIN_DIR/gatling.sh -rm local -s RinhaBackendCrebitosSimulation \
              -rd "Rinha de Backend - 2024/Q1: Crébito" \
              -rf $RESULTS_WORKSPACE \
              -sf "$GATLING_WORKSPACE/simulations"
      }

      startTest() {
          for i in {1..20}; do
              # 2 requests to wake the 2 api instances up :)
              curl --fail http://localhost:9999/clientes/1/extrato && \
              echo "" && \
              curl --fail http://localhost:9999/clientes/1/extrato && \
              echo "" && \
              runGatling && \
              break || sleep 2;
          done
      }

      startTest

      import scala.concurrent.duration._

      import scala.util.Random

      import util.Try

      import io.gatling.commons.validation._
      import io.gatling.core.session.Session
      import io.gatling.core.Predef._
      import io.gatling.http.Predef._


      class RinhaBackendCrebitosSimulation
        extends Simulation {

        def randomClienteId() = Random.between(1, 5 + 1)
        def randomValorTransacao() = Random.between(1, 10000 + 1)
        def randomDescricao() = Random.alphanumeric.take(10).mkString
        def randomTipoTransacao() = Seq("c", "d", "d")(Random.between(0, 2 + 1)) // not used
        def toInt(s: String): Option[Int] = {
          try {
            Some(s.toInt)
          } catch {
            case e: Exception => None
          }
        }

        val validarConsistenciaSaldoLimite = (valor: Option[String], session: Session) => {
          /*
            Essa função é frágil porque depende que haja uma entrada
            chamada 'limite' com valor conversível para int na session
            e também que seja encadeada com com jmesPath("saldo") para
            que 'valor' seja o primeiro argumento da função validadora
            de 'validate(.., ..)'.
            
            =============================================================
            
            Nota para quem não tem experiência em testes de performance:
              O teste de lógica de saldo/limite extrapola o que é comumente 
              feito em testes de performance apenas por causa da natureza
              da Rinha de Backend. Evite fazer esse tipo de coisa em 
              testes de performance, pois não é uma prática recomendada
              normalmente.
          */ 

          val saldo = valor.flatMap(s => Try(s.toInt).toOption)
          val limite = toInt(session("limite").as[String])

          (saldo, limite) match {
            case (Some(s), Some(l)) if s.toInt < l.toInt * -1 => Failure("Limite ultrapassado!")
            case (Some(s), Some(l)) if s.toInt >= l.toInt * -1 => Success(Option("ok"))
            case _ => Failure("WTF?!")
          }
        }

        val httpProtocol = http
          .baseUrl("http://localhost:9999")
          .userAgentHeader("Agente do Caos - 2024/Q1")

        val debitos = scenario("débitos")
          .exec {s =>
            val descricao = randomDescricao()
            val cliente_id = randomClienteId()
            val valor = randomValorTransacao()
            val payload = s"""{"valor": ${valor}, "tipo": "d", "descricao": "${descricao}"}"""
            val session = s.setAll(Map("descricao" -> descricao, "cliente_id" -> cliente_id, "payload" -> payload))
            session
          }
          .exec(
            http("débitos")
            .post(s => s"/clientes/${s("cliente_id").as[String]}/transacoes")
                .header("content-type", "application/json")
                .body(StringBody(s => s("payload").as[String]))
                .check(
                  status.in(200, 422),
                  status.saveAs("httpStatus"))
                .checkIf(s => s("httpStatus").as[String] == "200") { jmesPath("limite").saveAs("limite") }
                .checkIf(s => s("httpStatus").as[String] == "200") {
                  jmesPath("saldo").validate("ConsistenciaSaldoLimite - Transação", validarConsistenciaSaldoLimite)
                }
          )

        val creditos = scenario("créditos")
          .exec {s =>
            val descricao = randomDescricao()
            val cliente_id = randomClienteId()
            val valor = randomValorTransacao()
            val payload = s"""{"valor": ${valor}, "tipo": "c", "descricao": "${descricao}"}"""
            val session = s.setAll(Map("descricao" -> descricao, "cliente_id" -> cliente_id, "payload" -> payload))
            session
          }
          .exec(
            http("créditos")
            .post(s => s"/clientes/${s("cliente_id").as[String]}/transacoes")
                .header("content-type", "application/json")
                .body(StringBody(s => s("payload").as[String]))
                .check(
                  status.in(200),
                  jmesPath("limite").saveAs("limite"),
                  jmesPath("saldo").validate("ConsistenciaSaldoLimite - Transação", validarConsistenciaSaldoLimite)
                )
          )

        val extratos = scenario("extratos")
          .exec(
            http("extratos")
            .get(s => s"/clientes/${randomClienteId()}/extrato")
            .check(
              jmesPath("saldo.limite").saveAs("limite"),
              jmesPath("saldo.total").validate("ConsistenciaSaldoLimite - Extrato", validarConsistenciaSaldoLimite)
          )
        )

        val validacaConcorrentesNumRequests = 25
        val validacaoTransacoesConcorrentes = (tipo: String) =>
          scenario(s"validação concorrência transações - ${tipo}")
          .exec(
            http("validações")
            .post(s"/clientes/1/transacoes")
                .header("content-type", "application/json")
                .body(StringBody(s"""{"valor": 1, "tipo": "${tipo}", "descricao": "validacao"}"""))
                .check(status.is(200))
          )
        
        val validacaoTransacoesConcorrentesSaldo = (saldoEsperado: Int) =>
          scenario(s"validação concorrência saldo - ${saldoEsperado}")
          .exec(
            http("validações")
            .get(s"/clientes/1/extrato")
            .check(
              jmesPath("saldo.total").ofType[Int].is(saldoEsperado)
            )
          )

        val saldosIniciaisClientes = Array(
          Map("id" -> 1, "limite" ->   1000 * 100),
          Map("id" -> 2, "limite" ->    800 * 100),
          Map("id" -> 3, "limite" ->  10000 * 100),
          Map("id" -> 4, "limite" -> 100000 * 100),
          Map("id" -> 5, "limite" ->   5000 * 100),
        )

        val criterioClienteNaoEcontrado = scenario("validação HTTP 404")
          .exec(
            http("validações")
            .get("/clientes/6/extrato")
            .check(status.is(404))
          )

        val criteriosClientes = scenario("validações")
          .feed(saldosIniciaisClientes)
          .exec(
            /*
              Os valores de http(...) essão duplicados propositalmente
              para que sejam agrupados no relatório e ocupem menos espaço.
              O lado negativo é que, em caso de falha, pode não ser possível
              saber sua causa exata.
            */ 
            http("validações")
            .get("/clientes/#{id}/extrato")
            .check(
              status.is(200),
              jmesPath("saldo.limite").ofType[String].is("#{limite}"),
              jmesPath("saldo.total").ofType[String].is("0")
            )
          )
          .exec(
            http("validações")
            .post("/clientes/#{id}/transacoes")
                .header("content-type", "application/json")
                .body(StringBody(s"""{"valor": 1, "tipo": "c", "descricao": "toma"}"""))
                .check(
                  status.in(200),
                  jmesPath("limite").saveAs("limite"),
                  jmesPath("saldo").validate("ConsistenciaSaldoLimite - Transação", validarConsistenciaSaldoLimite)
                )
          )
          .exec(
            http("validações")
            .post("/clientes/#{id}/transacoes")
                .header("content-type", "application/json")
                .body(StringBody(s"""{"valor": 1, "tipo": "d", "descricao": "devolve"}"""))
                .check(
                  status.in(200),
                  jmesPath("limite").saveAs("limite"),
                  jmesPath("saldo").validate("ConsistenciaSaldoLimite - Transação", validarConsistenciaSaldoLimite)
                )
          )
          .exec(
            http("validações")
            .get("/clientes/#{id}/extrato")
            .check(
              jmesPath("ultimas_transacoes[0].descricao").ofType[String].is("devolve"),
              jmesPath("ultimas_transacoes[0].tipo").ofType[String].is("d"),
              jmesPath("ultimas_transacoes[0].valor").ofType[Int].is("1"),
              jmesPath("ultimas_transacoes[1].descricao").ofType[String].is("toma"),
              jmesPath("ultimas_transacoes[1].tipo").ofType[String].is("c"),
              jmesPath("ultimas_transacoes[1].valor").ofType[Int].is("1")
            )
          )
          .exec( // Consistencia do extrato
            http("validações")
            .post("/clientes/#{id}/transacoes")
                .header("content-type", "application/json")
                .body(StringBody(s"""{"valor": 1, "tipo": "c", "descricao": "danada"}"""))
                .check(
                  status.in(200),
                  jmesPath("saldo").saveAs("saldo"),
                  jmesPath("limite").saveAs("limite")
                )
                .resources(
                  // 5 consultas simultâneas ao extrato para verificar consistência
                  http("validações").get("/clientes/#{id}/extrato").check(
                    jmesPath("ultimas_transacoes[0].descricao").ofType[String].is("danada"),
                    jmesPath("ultimas_transacoes[0].tipo").ofType[String].is("c"),
                    jmesPath("ultimas_transacoes[0].valor").ofType[Int].is("1"),
                    jmesPath("saldo.limite").ofType[String].is("#{limite}"),
                    jmesPath("saldo.total").ofType[String].is("#{saldo}")
                  ),
                  http("validações").get("/clientes/#{id}/extrato").check(
                    jmesPath("ultimas_transacoes[0].descricao").ofType[String].is("danada"),
                    jmesPath("ultimas_transacoes[0].tipo").ofType[String].is("c"),
                    jmesPath("ultimas_transacoes[0].valor").ofType[Int].is("1"),
                    jmesPath("saldo.limite").ofType[String].is("#{limite}"),
                    jmesPath("saldo.total").ofType[String].is("#{saldo}")
                  ),
                  http("validações").get("/clientes/#{id}/extrato").check(
                    jmesPath("ultimas_transacoes[0].descricao").ofType[String].is("danada"),
                    jmesPath("ultimas_transacoes[0].tipo").ofType[String].is("c"),
                    jmesPath("ultimas_transacoes[0].valor").ofType[Int].is("1"),
                    jmesPath("saldo.limite").ofType[String].is("#{limite}"),
                    jmesPath("saldo.total").ofType[String].is("#{saldo}")
                  ),
                  http("validações").get("/clientes/#{id}/extrato").check(
                    jmesPath("ultimas_transacoes[0].descricao").ofType[String].is("danada"),
                    jmesPath("ultimas_transacoes[0].tipo").ofType[String].is("c"),
                    jmesPath("ultimas_transacoes[0].valor").ofType[Int].is("1"),
                    jmesPath("saldo.limite").ofType[String].is("#{limite}"),
                    jmesPath("saldo.total").ofType[String].is("#{saldo}")
                  )
              )
          )
        
        .exec(
            http("validações")
            .post("/clientes/#{id}/transacoes")
                .header("content-type", "application/json")
                .body(StringBody(s"""{"valor": 1.2, "tipo": "d", "descricao": "devolve"}"""))
                .check(status.in(422, 400))
          )
          .exec(
            http("validações")
            .post("/clientes/#{id}/transacoes")
                .header("content-type", "application/json")
                .body(StringBody(s"""{"valor": 1, "tipo": "x", "descricao": "devolve"}"""))
                .check(status.in(422))
          )
          .exec(
            http("validações")
            .post("/clientes/#{id}/transacoes")
                .header("content-type", "application/json")
                .body(StringBody(s"""{"valor": 1, "tipo": "c", "descricao": "123456789 e mais um pouco"}"""))
                .check(status.in(422))
          )
          .exec(
            http("validações")
            .post("/clientes/#{id}/transacoes")
                .header("content-type", "application/json")
                .body(StringBody(s"""{"valor": 1, "tipo": "c", "descricao": ""}"""))
                .check(status.in(422))
          )
          .exec(
            http("validações")
            .post("/clientes/#{id}/transacoes")
                .header("content-type", "application/json")
                .body(StringBody(s"""{"valor": 1, "tipo": "c", "descricao": null}"""))
                .check(status.in(422))
          )

        /* 
          Separar créditos e débitos dá uma visão
          melhor sobre como as duas operações se
          comportam individualmente.
        */
        setUp(
          validacaoTransacoesConcorrentes("d").inject(
            atOnceUsers(validacaConcorrentesNumRequests)
          ).andThen(
            validacaoTransacoesConcorrentesSaldo(validacaConcorrentesNumRequests * -1).inject(
              atOnceUsers(1)
            )
          ).andThen(
            validacaoTransacoesConcorrentes("c").inject(
              atOnceUsers(validacaConcorrentesNumRequests)
            ).andThen(
              validacaoTransacoesConcorrentesSaldo(0).inject(
                atOnceUsers(1)
              )
            )
          ).andThen(
            criteriosClientes.inject(
              atOnceUsers(saldosIniciaisClientes.length)
            ),
            criterioClienteNaoEcontrado.inject(
              atOnceUsers(1)
            ).andThen(
              debitos.inject(
                rampUsersPerSec(1).to(220).during(2.minutes),
                constantUsersPerSec(220).during(2.minutes)
              ),
              creditos.inject(
                rampUsersPerSec(1).to(110).during(2.minutes),
                constantUsersPerSec(110).during(2.minutes)
              ),
              extratos.inject(
                rampUsersPerSec(1).to(10).during(2.minutes),
                constantUsersPerSec(10).during(2.minutes)
              )
            )
          )
        ).protocols(httpProtocol)
      }
