#rabodegalo/Dockerfile
# Use a imagem base do Bash
FROM bash:latest

# Copie os arquivos de código para o contêiner
COPY . /app

# Defina o diretório de trabalho
WORKDIR /app

# Comando padrão para iniciar a API
CMD ["bash", "main.sh"]
