#rabodegalo/app/main.sh
#!/bin/bash

# Define o PID do processo atual
PID=$$

# Cria o FIFO para comunicação entre processos
FIFO_PATH="/tmp/pid-$PID/response"
rm -f $FIFO_PATH
mkfifo $FIFO_PATH

# Importa as funções de manipulação de solicitações
source request_handler.sh

# Valida entradas antes de chamar a função sensível
if [[ -n "${session["valor"]}" && -n "${session["limite"]}" ]]; then
    validarConsistenciaSaldoLimite session["valor"] session
else
    # Trate o caso em que 'valor' ou 'limite' estão ausentes na sessão
    echo "Valor ou limite ausentes na sessão"
fi

# Inicia o servidor
echo 'Listening on 3000...'
nc -lN 3000 < $FIFO_PATH | while read -r request; do
    handleRequest "$request" > $FIFO_PATH &
done
