
#rabodegalo/config/init.sql

-- Criação da tabela de contas
CREATE TABLE IF NOT EXISTS accounts (
    id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    limit_amount INTEGER NOT NULL
);

-- Criação da tabela de transações
CREATE TABLE IF NOT EXISTS transactions (
    id SERIAL PRIMARY KEY,
    account_id INTEGER NOT NULL,
    amount INTEGER NOT NULL,
    transaction_type CHAR(1) NOT NULL,
    description VARCHAR(255) NOT NULL,
    date TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (account_id) REFERENCES accounts(id)
);

-- Criação da tabela de saldos
CREATE TABLE IF NOT EXISTS balances (
    account_id INTEGER PRIMARY KEY REFERENCES accounts(id),
    amount INTEGER NOT NULL,
    FOREIGN KEY (account_id) REFERENCES accounts(id)
);

-- Inicialização dos dados com o uso de transação
DO $$
BEGIN
    -- Inicia uma transação
    BEGIN
        -- Inserção de contas com seus limites
        INSERT INTO accounts (name, limit_amount)
        VALUES
            ('o barato sai caro', 100000),
            ('zan corp ltda', 80000),
            ('les cruders', 1000000),
            ('padaria joia de cocaia', 10000000),
            ('kid mais', 500000)
        RETURNING id;

        -- Inserção de saldos iniciais para cada conta
        INSERT INTO balances (account_id, amount)
        SELECT id, 0 FROM accounts;
    -- Finaliza a transação
    EXCEPTION
        WHEN others THEN
            -- Em caso de erro, faz rollback da transação
            ROLLBACK;
    END;
    -- Commit da transação
    COMMIT;
END $$;

