
#rabodegalo/app/transaction_handler.sh
#!/bin/bash

# Handles GET request for bank statement
function handle_GET_bank_statement() {
  local id="${PARAMS["id"]}"

  if [[ -n "$id" ]]; then
    local query="
      WITH ten_transactions AS (
        SELECT * FROM transactions 
        WHERE account_id = $id 
        ORDER BY date DESC
        LIMIT 10
      )
      SELECT 
        json_build_object('saldo', json_build_object(
          'total', balances.amount,
          'data_extrato', NOW()::date,
          'limite', accounts.limit_amount,
          'ultimas_transacoes', 
            CASE 
            WHEN COUNT(transactions) = 0 THEN '[]'
            ELSE
              json_agg(
                json_build_object(
                  'valor', transactions.amount,
                  'tipo', transactions.transaction_type,
                  'descricao', transactions.description,
                  'realizada_em', transactions.date::date
                )
              )
            END
        ))
      FROM accounts
      LEFT JOIN balances ON balances.account_id = accounts.id
      LEFT JOIN ten_transactions AS transactions ON transactions.account_id = accounts.id
      WHERE accounts.id = $id
      GROUP BY accounts.id, balances.amount, accounts.limit_amount
    "

    # Executa a consulta e processa a resposta
    local result=$(psql -t -h pgbouncer -U postgres -d postgres -p 6432 -c "$query" | tr -d '[:space:]') 

    if [[ -n "$result" ]]; then
      generate_response "$result"
    else
      handle_not_found
    fi
  fi
}


# Handles POST request for transactions
function handle_POST_transactions() {
  local id="${PARAMS["id"]}"
  local amount=$(jq -r '.valor' <<< "$BODY")
  local transaction_type=$(jq -r '.tipo' <<< "$BODY")
  local description=$(jq -r '.descricao' <<< "$BODY")

  local operation
  [[ "$transaction_type" == "c" ]] && operation="+" || operation="-"

  if [[ -n "$id" ]]; then
    local query="
      INSERT INTO transactions (account_id, amount, description, transaction_type)
      VALUES ($id, $amount, '$description', '$transaction_type');

      UPDATE balances
      SET amount = amount $operation $amount
      WHERE balances.account_id = $id;

      SELECT 
        json_build_object(
          'limite', accounts.limit_amount,
          'saldo', balances.amount
        )
      FROM accounts
      LEFT JOIN balances ON balances.account_id = accounts.id
      WHERE account_id = $id
    "

    # Executa a consulta e processa a resposta
    local result=$(psql -t -h pgbouncer -U postgres -d postgres -p 6432 -c "$query" | tr -d '[:space:]') 

    if [[ -n "$result" ]]; then
      generate_response "$result"
    else
      handle_not_found
    fi
  fi
}

function handle_GET_bank_statement() {
  ID=${PARAMS["id"]}

  if [ ! -z "$ID" ]; then
    # Sanitize input: ensure ID is numeric
    if ! [[ $ID =~ ^[0-9]+$ ]]; then
      handle_invalid_input "Invalid ID format"
      return
    fi

    QUERY="
WITH ten_transactions AS (
    SELECT * FROM transactions 
    WHERE account_id = $ID 
    ORDER BY date DESC
    LIMIT 10
)
SELECT 
  json_build_object('saldo', json_build_object(
    'total', balances.amount,
    'data_extrato', NOW()::date,
    'limite', accounts.limit_amount,
    'ultimas_transacoes', 
      CASE 
      WHEN COUNT(transactions) = 0 THEN '[]'
      ELSE
        json_agg(
          json_build_object(
            'valor', transactions.amount,
            'tipo', transactions.transaction_type,
            'descricao', transactions.description,
            'realizada_em', transactions.date::date
          )
        )
      END
  ))
FROM accounts
LEFT JOIN balances ON balances.account_id = accounts.id
LEFT JOIN ten_transactions AS transactions ON transactions.account_id = accounts.id
WHERE accounts.id = $ID
GROUP BY accounts.id, balances.amount, accounts.limit_amount"

    RESULT=$(psql -t -h pgbouncer -U postgres -d postgres -p 6432 -c "$QUERY" | tr -d '[:space:]') 

    if [ ! -z "$RESULT" ]; then
      generate_response "$RESULT"
    else
      handle_not_found
    fi
  fi
}


#rabodegalo/app/response_handler.sh
#!/bin/bash

# Generates response based on the result
function generate_response() {
  local result="$1"

  if [[ -n "$result" ]]; then
    RESPONSE=$(cat views/bank_statement.jsonr | sed "s/{{data}}/$result/")
  else
    RESPONSE=$(cat views/404.htmlr)
  fi
}

# Handles not found error
function handle_not_found() {
  RESPONSE=$(cat views/404.htmlr)
}

