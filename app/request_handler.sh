#rabodegalo/app/request_handler.sh
#!/bin/bash

# Parses the request and invokes appropriate handler
function handleRequest() {
  read -r -a request <<< "$1"

  # Parses the path parameter (integer)
  local path_regex='GET /clientes/([0-9]+)/extrato$'
  if [[ "${request[1]}" =~ $path_regex ]]; then
    PARAMS["id"]="${BASH_REMATCH[1]}"
    handle_GET_bank_statement
  elif [[ "${request[1]}" == "POST /clientes/:id/transacoes" ]]; then
    handle_POST_transactions
  else
    handle_not_found
  fi 
}


